Title: Security updates for the X Window System client libraries
Date: 2013-05-25 01:30
Tags: security, wheezy, squeeze
Slug: security-updates-for-xorg
Author: Ana Guerrero
Status: published

If you see a bunch of X.org packages upgrades pending in your Squeeze or brand new Wheezy system, don't panic! 

Ilja van Sprundel, a security researcher from IOActive, has discovered a large number of issues in the various X client libraries and he has worked with X.Org's security team to analyze, confirm, and fix these issues.
You can find [more information in the security advisory from X.org](http://www.x.org/wiki/Development/Security/Advisory-2013-05-23).

The Debian Security and X.org teams have quickly updated all the affected packages in Squeeze and Wheezy. You can see [the full list of updates in the debian-security-announce mailing list archives](https://lists.debian.org/debian-security-announce/2013/threads.html).
