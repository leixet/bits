Title: Debian announces partnership to sub-contract publicity and press to an outside marketing agency
Slug: publicity-agency
Date: 2016-04-01 07:10
Author: Debian Publicity Team
Tags: debian, publicity, announce
Status: published

Last year we started to push more of Debian news and information 
away the single news source of the [DPN](https://www.debian.org/News/weekly/) 
into other media services. Debian has been more active than ever on our many IRC channels, 
free software based social networks, and unofficial Twitter and Facebook feeds. 
Today we have decided to announce the next stage in keeping Debian at the forefront 
of media by sub-contracting publicity and press to an outside marketing agency.

The marketing agency (name will be disclosed soon) has provided an AI system 
(running entirely with free software) which will be fed with all the content 
of Debian mailing lists and [sources.debian.net](https://sources.debian.net/) 
to understand the character of the Debian community 
and then better customize future articles, interviews, and event news. 

However, some bits of personal information are also needed. 
Please install the "publicity" package and you'll be presented 
a form to fill in your data: name, surname, phone, snail mail address, 
place of birth, names of family members, employers or employees. 
Each person providing their data to the agency will receive coupon for a 20% discount 
in the download (purchase) of next Debian release 
(valid only for downloads from the official site www.debian.org).

We kindly ask every Debian  community member to sign up in, at least, 
one of theses services: Twitter, Whatsapp, Slack or Facebook 
(IRC, mailing lists, and free software based RTC are allegedly not so 'cool'). 
Users need not be concerned with losing the features that the IRC bots provide 
(so long [KGB](https://wiki.debian.org/Services/KGB)!) as they will be replaced 
by Tay-like AI systems. The most visible change will be that [MeetBot](https://wiki.debian.org/MeetBot) 
will no longer log the meetings anymore, but we have bribed an NSA employee 
so they pass the relevant messages to us.

"If this 'centralization, outsourcing and pay-and-forget' approach 
goes well with publicity, I'm considering running for DPL in 2017 
to extend this model to other areas of Debian" said Laura Arjona Reina, 
(now) former publicity delegate.

A new logo and mascot has been designed too, as a symbol of this new era 
embracing the standards of branding and corporate messaging. 
Please consider voting in favor of it, in the General Resolution that will be proposed soon:

[![Mascot and Logo](|filename|/images/deb_raccoon.png)][debraccoon-link]

[debraccoon-link]: http://deb.li/debraccoon
