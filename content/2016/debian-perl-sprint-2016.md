Title: Debian Perl Sprint 2016
Slug: debian-perl-sprint-2016
Date: 2016-07-06 23:45
Author: Alex Muntada
Tags: perl, sprint, zrh2016
Status: published


Six members of the [Debian Perl](https://wiki.debian.org/Teams/DebianPerlGroup) team met in Zurich over the weekend from May 19 to May 22 to continue the development around perl for Stretch and to work on QA across 3000+ packages.

The participants had a good time, met friends from local groups and even found some geocaches. Obviously, the [sprint](https://wiki.debian.org/Sprints/2016/DebianPerlSprint) was productive this time too:

* 36 bugs were filed or worked on, 28 uploads were accepted.
* The plan to get Perl 5.24 transition into Stretch was confirmed, and a [test rebuild server](http://perl.debian.net/) was set up.
* Cross building XS modules was demoed, and the conditions where it is viable were discussed.
* Several improvements were made in the team packaging tools, and new features were discussed and drafted.
* A talk on downstream distribution aimed at CPAN authors was proposed for [YAPC::EU 2016](http://act.yapc.eu/ye2016).

The [full report](https://lists.debian.org/debian-perl/2016/06/msg00027.html) was posted to the relevant Debian mailing lists.

The participants would like to thank the [ETH Zurich](https://www.ethz.ch/en.html) for hosting us, and all donors to the Debian project who helped to cover a large part of our expenses.
