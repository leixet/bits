Title: Debian está buscando tres becados para el Programa de extensión
Slug: outreachy-looking-for-three-applicants
Date: 2016-03-12 20:10
Author: Nicolas Dandrimont
Lang: es
Translator: Adrià García-Alzórriz
Tags: announce, outreachy
Status: draft

![Outreachy logo](|filename|/images/outreachy-logo-300x61.png)

Como parte de las iniciativas de extensión de su diversidad, Debian 
participará en la próxima doceava edición (de mayo a agosto de 2016) 
del [Outreachy][1], un programa de becas abierto a todas las mujeres 
del mundo (cis y trans), hombres trans y personas imbinarias, así como 
también nacidos y residentes en Estados Unidos de cualquier género que 
sean negros/americanos africanos, hispanos/latinos, indios americanos, 
nativos de Alaska, nativos hawaianos o habitantes de las Islas del
Pacífico.

[1]: https://www.gnome.org/outreachy/

Agradeciendo la generosidad de nuestros donantes, y especialmente a 
nuestro patrocinador [Intel](http://www.intel.com), que ha donado
fondos especialmente para un becario, Debian podrá acoger tres 
becarios esta vez.

Las candidaturas estarán abiertas hasta el 22 de marzo, así que ¡no 
esperes!
Debian tiene [muchas y muy interesantes oportunidades de becado este año][2].
Hay más información disponible sobre el programa en la 
[página específica de Debian del programa][3],
así como también en la [página web oficial][4]. No dudes en contactar
con el equipo de extensión y tutores a través de nuestra
[lista de correo][5] o de nuestro canal de IRC #debian-soc en irc.oftc.net

[2]: https://wiki.debian.org/SummerOfCode2016/Projects (es la misma lista que la del GSoC)
[3]: https://wiki.debian.org/Outreachy/Round12
[4]: https://wiki.gnome.org/Outreachy
[5]: https://lists.debian.org/debian-outreach/ (debian-outreach EN lists.debian.org)

Si quieres que Debian siga participando en programas de este tipo y 
amplíe los esfuerzos de extensión, puedes hacer una 
[donación a una de nuestras organizaciones que apoyan el proyecto Debian ][7], 
o haciéndote voluntario en algún momento participando en discusiones 
en nuestra lista de correo.

[7]: https://www.debian.org/donations
