Title: Debian 9.0 Stretch wurde freigegeben!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: de
Translator: Holger Wansing

![Alt Stretch wurde freigegeben](|filename|/images/banner_stretch.png)

Lassen Sie sich von dem lila Spielzeug-Oktopus umarmen!
Wir freuen uns, die Freigabe von
Debian 9.0, Codename *Stretch*, bekanntgeben zu können.

**Sie möchten es installieren?**
Wählen Sie Ihr bevorzugtes [Installationsmedium](https://www.debian.org/distrib/), z.B.  Blu-ray-Disk, DVD,
CD oder USB-Stick. Lesen Sie dann die [Installationsanleitung](https://www.debian.org/releases/stretch/installmanual).

**Sie sind bereits ein zufriedener Debian-Nutzer und möchten
lediglich Ihr System aktualisieren ?**
Sie können ganz einfach ein Upgrade Ihrer derzeitigen
Debian-8-Installation (Jessie) durchführen,
bitte lesen Sie die [Veröffentlichungshinweise](https://www.debian.org/releases/stretch/releasenotes).

**Sie möchten die Freude an der Veröffentlichung von Debian Stretch teilen?**
Binden Sie den [Banner aus diesem Blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) in Ihren Blog oder Ihre Website ein!
