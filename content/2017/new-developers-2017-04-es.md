Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2017)
Slug: new-developers-2017-04
Date: 2017-05-15 12:30
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Status: published

Los siguientes colaboradores del proyecto se convirtieron en Debian Developers
en los dos últimos meses:

  * Guilhem Moulin (guilhem)
  * Lisa Baron (jeffity)
  * Punit Agrawal (punit)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers
en los dos últimos meses:

  * Sebastien Jodogne
  * Félix Lechner
  * Uli Scholler
  * Aurélien Couderc
  * Ondřej Kobližek
  * Patricio Paez

¡Felicidades a todos!
