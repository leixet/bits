Title: La programmation de DebConf17 enfin disponible
Slug: dc17-schedule
Date: 2017-07-25 01:15
Author: DebConf team
Tags: debconf17, debconf, openday
Status: published
Lang: fr

L'équipe d'organisation de DebConf17 est fière d'annoncer que plus de
120 activités ont été placées à l'horaire jusqu'ici. Ceci inclue des
présentations de 20 et de 45 minutes, des rencontres d'équipe, des
ateliers de travail et d'autres types d'activités.

Grâce à notre équipe vidéo, la majorité des présentations et BoFs seront
enregistrées et diffusées en ligne!

Nous aimerions également vous rappeler que le samedi 5 août se tiendra
la journée portes ouvertes! Cette journée offrira une programmation
adaptée à une audience plus large et inclut des activités pour
nouveaux/nouvelles telles qu'une séance de questions-réponses sur le
Projet Debian, un atelier d'empaquetage pour débutants, une présentation
sur les libertés entourant les appareils "intelligents" populaires et
bien plus.

En plus de l'horaire déjà publié, certaines salles seront disponibles
pour des sessions ad-hoc où les participants/participantes pourront
planifier des activités libres n'importe quand durant toute la conférence.

L'horaire actuel est disponible au [https://debconf17.debconf.org/schedule/](https://debconf17.debconf.org/schedule/).

Il est également possible d'accéder à une version XML de l'horaire que
vous pouvez donner à ConfClerk dans Debian, ou alors à Giggity sur Android: [https://debconf17.debconf.org/schedule/mobile/](https://debconf17.debconf.org/schedule/mobile/).

Au plaisir de vous voir à Montréal!

![DebConf17 logo](|filename|/images/800px-Dc17logo.png)
