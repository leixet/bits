Title: Debian Artwork: Call for Proposals for Debian 10 (Buster)
Date: 2018-06-17 13:30
Tags: artwork, buster, cfp, debian
Slug: buster-artwork-cfp
Author: Jonathan Carter
Status: published

This is the official call for artwork proposals for the Buster cycle.

For the most up to date details, please refer to the
[wiki](https://wiki.debian.org/DebianDesktop/Artwork/Buster).

We would also like to take this opportunity to thank Juliette
Taka Belin for doing the
[Softwaves theme for stretch](https://wiki.debian.org/DebianArt/Themes/softWaves).

The deadlines for submissions is: 2018-09-05

The artwork is usually picked based on which themes look the most:

* ''Debian'': admittedly not the most defined concept, since everyone has their
  own take on what Debian means to them.
* ''plausible to integrate without patching core software'':
  as much as we love some of the insanely hot looking themes, some
  would require heavy GTK+ theming and patching GDM/GNOME.
* ''clean / well designed'': without becoming something that gets annoying to look at a
  year down the road.  Examples of good themes include Joy,
  Lines and Softwaves.

If you'd like more information, please use the 
[Debian Desktop mailing list](https://lists.debian.org/debian-desktop/).
