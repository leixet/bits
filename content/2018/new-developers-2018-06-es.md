Title: Nuevos desarrolladores y mantenedores de Debian (mayo y junio de 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: Jean-Pierre Giraud y Laura Arjona Reina 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Andre Bianchi
  * Simon Quigley
  * Andrius Merkys
  * Tong Sun
  * James Lu
  * Raphaël Halimi
  * Paul Seyfert
  * Dustin Kirkland
  * Yanhao Mo
  * Paride Legovini

¡Felicidades a todos!


Se necesitan gestores de solicitud («Application Managers») para ayudar a los colaboradores del proyecto a convertirse en Desarrolladores de Debian.
Más detalles en la [convocatoria del equipo de Recepción de Nuevos Miembros](https://lists.debian.org/debian-devel-announce/2018/06/msg00007.html).
