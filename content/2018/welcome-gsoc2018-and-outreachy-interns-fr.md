Title: Debian accueille ses stagiaires de GSoC 2018 et d'Outreachy
Slug: welcome-gsoc2018-and-outreachy-interns
Date: 2018-05-30 20:45
Author: Laura Arjona Reina
Tags: announce, gsoc, outreachy
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

![GSoC logo](|filename|/images/gsoc.jpg) 

![Outreachy logo](|filename|/images/outreachy-logo-300x61.png)

Nous sommes heureux de vous annoncer que Debian a sélectionné vingt-six stagiaires
pour travailler avec nous durant les six prochains mois : une personne pour
[Outreachy][1] et vingt-cinq pour le [Google Summer of Code][2].

[1]: https://www.outreachy.org/alums/
[2]: https://summerofcode.withgoogle.com/organizations/5166394929315840/?sp-page=3#!

Voici la liste des projets et des stagiaires qui vont travailler dessus :

[Une base de données d'agenda pour les événements sociaux et les conférences](https://wiki.debian.org/SocialEventAndConferenceCalendars)

* [Doğukan Çelik](https://www.dogukancelik.com)

[Les outils du kit de développement d'Android dans Debian](https://wiki.debian.org/AndroidTools)

* [Saif Abdul Cassim](https://salsa.debian.org/m36-guest) (GSoC 2018, les outils du kit de développement d'Android dans Debian)
* [Chandramouli Rajagopalan](https://salsa.debian.org/r_chandramouli-guest) (empaquetage et mise à jour des outils du kit de développement d'Android)
* [Umang Parmar](https://salsa.debian.org/darkLord-guest) (les outils du kit de développement d'Android dans Debian)

[Constructions automatiques avec clang en utilisant OBS](https://wiki.debian.org/SummerOfCode2018/Projects/ClangRebuild)

* [Athos Ribeiro](https://athoscr.me/blog/gsoc2018-1/)

[Paquets automatiques pour tout](https://wiki.debian.org/SummerOfCode2018/Projects/AutomaticPackagesForEverything)

* [Alexandre Viau](https://salsa.debian.org/aviau)

[Une fenêtre contextuelle « cliquer pour téléphoner » pour le bureau Linux](https://wiki.debian.org/SummerOfCode2018/Projects/ClickToDialPopupWindowForLinuxDesktop)

* [Diellza Shabani](https://wiki.debian.org/DiellzaShabani)
* [Sanjay Prajapat](https://salsa.debian.org/sanjaypra555-guest/)
* [Vishal Gupta](https://salsa.debian.org/comfortablydumb-guest/)

[Conception et mise en œuvre d'une solution d'authentification unique (SSO) pour Debian](https://wiki.debian.org/SummerOfCode2018/Projects/NewDebianSSO)

* [Birger Schacht](https://bisco.org/tags/gsoc18/)

[Améliorations d'EasyGnuPG](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Divesh Uttamchandani](https://wiki.debian.org/DiveshUttamchandani)


[Extraction de données à partir de factures et de comptes en PDF pour la comptabilité financière](https://wiki.debian.org/SummerOfCode2018/Projects/ExtractingDataFromPDFInvoicesAndBills)

* [Harshit Joshi](https://wiki.debian.org/HarshitJoshi)

[Greffon Firefox et Thunderbird pour les pratiques du logiciel libre](https://wiki.debian.org/SummerOfCode2018/Projects/FirefoxAndThunderbirdPluginFreeSoftwareHabits)

* [Enkelena Haxhija](https://wiki.debian.org/EnkelenaHaxhija)

[Application graphique pour EasyGnuPG](https://wiki.debian.org/SummerOfCode2018/Projects/EasyGnuPG)

* [Yugesh Ajit Kothari](https://github.com/yugeshk)

[Amélioration de Distro Tracker pour un meilleur soutien aux équipes Debian](https://wiki.debian.org/SummerOfCode2018/Projects/ImprovingDistro-trackerToBetterSupportDebianTeams)

* [Arthur Del Esposte](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ArthurEsposte)

[Tableau Kanban pour le système de suivi des bogues de Debian et les serveurs CalDAV](https://wiki.debian.org/SummerOfCode2018/Projects/KanbanBoardForDebianBugTrackerAndCalDAVServer)

* [Chikirou Massiwayne](https://wiki.debian.org/SummerOfCode2018/StudentApplications/ChikirouMassiwayne)

[Améliorations d'OwnMailbox](https://wiki.debian.org/SummerOfCode2018/Projects/OwnMailbox)

* [Georgios Pipilis](https://github.com/giorgos314)

[Démarrage par le réseau pair à pair avec BitTorrent](https://wiki.debian.org/SummerOfCode2018/Projects/BootTorrent)

* [Shreyansh Khajanchi](https://github.com/shreyanshk)


[CD autonome isolé pour PGP](https://wiki.debian.org/SummerOfCode2018/Projects/PGPCleanRoomKeyManagement)
* [Jacob Adams](https://wiki.debian.org/JacobAdams)

[Portage des paquets de Kali pour Debian](https://wiki.debian.org/SummerOfCode2018/Projects/PortKaliPackages)

* [Samuel Henrique](https://salsa.debian.org/samueloph)

[Assurance qualité pour les applications à vocation biologique dans Debian](https://wiki.debian.org/SummerOfCode2018/Projects/QualityAssuranceForBiologicalApplicationsInsideDebian)

* [Liubov Chuprikova](https://salsa.debian.org/liubovch-guest)

[Ingénierie inverse pour les vannes thermostatiques Bluetooth de radiateur](https://wiki.debian.org/SummerOfCode2018/Projects/RadiatorThermovalveReverseEngineering)

* [Sergio Alberti](https://gitlab.com/sergioalberti)

[Serveur virtuel LTSP](https://wiki.debian.org/SummerOfCode2018/Projects/VirtualLtspServer)

* [Deepanshu Gajbhiye](https://github.com/d78ui98)

[Guide et interface graphique pour aider les étudiants et stagiaires à postuler et à démarrer](https://wiki.debian.org/NewContributorWizard)

* [Elena Gjevukaj](https://wiki.debian.org/ElenaGjevukaj)
* [Minkush Jain](https://wiki.debian.org/MinkushJain)
* [Shashank Kumar](https://wiki.debian.org/ShashankKumar)

Félicitations et bienvenue à tous !

Les programmes Google Summer of Code et Outreachy sont possibles dans Debian
grâce à l'effort de développeurs et de contributeurs de Debian qui consacrent
une partie de leur temps libre à encadrer les stagiaires et aux tâches de
diffusion.

Rejoignez-nous pour contribuer à développer Debian ! Vous pouvez suivre les rapports
hebdomadaires des stagiaires sur la [liste de diffusion debian-outreach][debian-outreach-ml],
dialoguer avec nous sur [notre canal IRC][debian-outreach-irc] ou la liste de
diffusion de chacun des projets.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/#debian-outreach (#debian-outreach sur irc.debian.org)
