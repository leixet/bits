Title: DebConf18 今日在新竹正式开始
Slug: dc18-starts
Date: 2018-07-29 03:30
Author: Laura Arjona Reina
Lang: zh-CN
Translator: Boyuan Yang
Tags: debconf18, debconf
Status: published

![DebConf18 logo](|filename|/images/DebConf18_Horizontal_Logo_600_240.png)

[DebConf18](https://debconf18.debconf.org/)，Debian 的第十九次会议，
正在台湾新竹进行；会议从七月二十九日开始，到八月五日结束。

在先前的一周中，来自世界各地的 Debian 贡献者在
[国立交通大学电子资讯研究大楼（NCTU MIRC）](https://wiki.debconf.org/wiki/DebConf18/Venue) 
会聚一堂参加了 DebCamp 活动（主要关注个人贡献和个体之间合作开发 Debian
的团队协作），另有开放日活动已于七月二十八日进行（包含公开演讲、演示和针对大众的工作坊活动）。

从今天起，主要的会议将正式开始，共有超过三百位与会人员将参加 118
项计划的活动，包括 45 分钟时长和 20 分钟时长的演讲和团队会议、
工作坊、一次招聘会、来自各个受邀人士的特约演讲以及各类其它活动事项。

完整的日程表可在
[https://debconf18.debconf.org/schedule/](https://debconf18.debconf.org/schedule/)
找到，该页面每日进行更新，包括整个会议中与会人员临时决定的活动。

如果您打算远程进行参与，您可以追踪
[DebConf18 网站上提供的 **视频串流**](https://debconf18.debconf.org/) ，
总共提供了三个演讲室内活动的视频直播：
Yushan (玉山)（主会堂）、Xueshan (雪山)和 Zhongyangjianshan (中央尖山)；
除此之外，也可加入在线聊天室：
  [**#debconf18-y**](https://webchat.oftc.net/?channels=#debconf18-y)、
  [**#debconf18-x**](https://webchat.oftc.net/?channels=#debconf18-x) 和
  [**#debconf18-z**](https://webchat.oftc.net/?channels=#debconf18-z)
（这些频道都位于 OFTC IRC 网络）。

DebConf 致力于为所有参与者提供安全而友好的环境。
请参见 [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) 
和 [Debian Code of Conduct](https://www.debian.org/code_of_conduct) 以了解更多信息。

Debian 感谢各位[赞助商](https://debconf18.debconf.org/sponsors/) 
对 DebConf18 的支持，尤其对我们的白金赞助商
[**Hewlett Packard Enterprise**](http://www.hpe.com/engage/opensource)
致谢。
