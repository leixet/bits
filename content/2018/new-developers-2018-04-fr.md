Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Andreas Boll (aboll)
  * Dominik George (natureshadow)
  * Julien Puydt (jpuydt)
  * Sergio Durigan Junior (sergiodj)
  * Robie Basak (rbasak)
  * Elena Grandi (valhalla)
  * Peter Pentchev (roam)
  * Samuel Henrique (samueloph)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Andy Li
  * Alexandre Rossi
  * David Mohammed
  * Tim Lunn
  * Rebecca Natalie Palmer
  * Andrea Bolognani
  * Toke Høiland-Jørgensen
  * Gabriel F. T. Gomes
  * Bjorn Anders Dolk
  * Geoffroy Youri Berret
  * Dmitry Eremin-Solenikov

Félicitations !




