Title: Novos desenvolvedores e mantenedores Debian (maio e junho de 2018)
Slug: new-developers-2018-06
Date: 2018-07-30 19:45
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: draft


Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos últimos dois meses:

  * Andre Bianchi
  * Simon Quigley
  * Andrius Merkys
  * Tong Sun
  * James Lu
  * Raphaël Halimi
  * Paul Seyfert
  * Dustin Kirkland
  * Yanhao Mo
  * Paride Legovini

Parabéns a todos!

