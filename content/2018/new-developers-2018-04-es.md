Title: Nuevos desarrolladores y mantenedores de Debian (marzo y abril del 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Andreas Boll (aboll)
  * Dominik George (natureshadow)
  * Julien Puydt (jpuydt)
  * Sergio Durigan Junior (sergiodj)
  * Robie Basak (rbasak)
  * Elena Grandi (valhalla)
  * Peter Pentchev (roam)
  * Samuel Henrique (samueloph)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Andy Li
  * Alexandre Rossi
  * David Mohammed
  * Tim Lunn
  * Rebecca Natalie Palmer
  * Andrea Bolognani
  * Toke Høiland-Jørgensen
  * Gabriel F. T. Gomes
  * Bjorn Anders Dolk
  * Geoffroy Youri Berret
  * Dmitry Eremin-Solenikov

¡Felicidades a todos!




