Title: DebConf18 thanks its sponsors!
Slug: dc18-thanks-its-sponsors
Date: 2018-00-01 09:15
Author: Laura Arjona Reina
Tags: debconf18, debconf, sponsors
Status: draft

![DebConf18 logo](|filename|/images/DebConf18_Horizontal_Logo_600_240.png)

DebConf18 is taking place in Hsinchu, Taiwan, from July 29th to August 5th, 2018. 
It is the first Debian Annual Conference in Asia, and we anticipate 
over 300 attendees and major advances for Debian and for Free Software in general.
We extend an invitation to everyone to join us and to support this event. 
As a volunteer-run non-profit conference, we depend on our sponsors.

Thirty-two companies have committed to sponsor DebConf18! With a warm
"thank you", we'd like to introduce them to you.

Our Platinum sponsor is  [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource).
HPE is an industry-leading technology company
providing a comprehensive portfolio of products such as 
integrated systems, servers, storage, networking and software.
The company offers consulting, operational support, financial services,
and complete solutions for many different industries: mobile and IoT, 
data & analytics and the manufacturing or public sectors among others.

HPE is also a development partner of Debian, 
and providing hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

We have four Gold sponsors:

* [**Google**](http://google.com), the technology company 
specialized in Internet-related services as online advertising and search engine,
* [**Infomaniak**](https://www.infomaniak.com/), Switzerland's largest web-hosting company, 
also offering live-streaming and video on demand services,
* [**Collabora**](https://www.collabora.com/), 
which offers a comprehensive range of services to help its clients to
navigate the ever-evolving world of Open Source, and
* [**Microsoft**](https://www.microsoft.com/), the American multinational technology company 
developing, licensing and selling computer software, consumer electronics, 
personal computers and related services. 


As Silver sponsors we have 
[**credativ**](http://www.credativ.de/) 
(a service-oriented company focusing on open-source software and also a 
[Debian development partner](https://www.debian.org/partners/)), 
[**Gandi**](https://www.gandi.net/),
FIXME-GANDI-DESCRIPTION,
[**Skymizer**](https://skymizer.com/)
FIXME-SKYMIZER-DESCRIPTION,
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
FIXME-CIP-DESCRIPTION,
[**Brandorr Group**](https://www.brandorr.com/),
FIXME-BRANDORR-DESCRIPTION,
[**3CX**](https://www.3cx.com/),
FIXME-3CX-DESCRIPTION,
[**Free Software Initiative Japan**](https://www.fsij.org/),
FIXME-FSIJ-DESCRIPTION,
[**Texas Instruments**](http://www.ti.com/) (the global semiconductor company),
the [**Bern University of Applied Sciences**](https://www.bfh.ch/) 
(with over [6,800](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled, 
located in the Swiss capital),
[**ARM**](),
FIXME-Description,
[**Ubuntu**](),
(the OS delivered by Canonical),
[**Cumulus Networks**](),
FIXME-Description,
[**Roche**](https://debconf18.debconf.org/media/sponsors_files/roche.svg),
(a major international pharmaceutical provider and research company 
dedicated to personalized healthcare),
and [**Hudson-Trading**](),
FIXME-Description.


[**ISG.EE**](http://isg.ee.ethz.ch/), 
[**Univention**](https://www.univention.com/)
[**Private Internet Access**](https://privateinternetaccess.com/),
[**Logilab**](https://www.logilab.fr/),
[**Dropbox**](https://www.dropbox.com/) and
[**IBM**](https://www.ibm.com/)
are our Bronze sponsors.

And finally, [**SLAT (Software Liberty Association of Taiwan)**](https://slat.org/),
[**The Linux foundation**](https://www.linuxfoundation.org/),
[**deepin**](https://www.deepin.com/),
[**Altus Metrum**](https://altusmetrum.org/),
[**Evolix**](https://evolix.com/),
[**BerryNet**](https://github.com/DT42/BerryNet) and
[**Purism**](https://puri.sm/)
are our supporter sponsors.

